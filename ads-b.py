#ADS-B
#write by hh
#170912

import binascii
import struct
import string
#coding: UTF-8
data_fifo=[0]*1280
pulse_amp = [0] * 112
confi = [0] * 112
correct=[0]*24
syndrome={
              0x3935EA,0x1C9AF5,0xF1B77E,0x78DBBF,0xC397DB,0x9E31E9,0xB0E2F0,0x587178,
              0x2C38BC,0x161C5E,0x0B0E2F,0xFA7D13,0x82C48D,0xBE9842,0x5F4C21,0xD05C14,
              0x682E0A,0x341705,0xE5F186,0x72F8C3,0xC68665,0x9CB936,0x4E5C9B,0xD8D449,
              0x939020,0x49C810,0x24E408,0x127204,0x093902,0x049C81,0xFDB444,0x7EDA22,
              0x3F6D11,0xE04C8C,0x702646,0x381323,0xE3F395,0x8E03CE,0x4701E7,0xDC7AF7,
              0x91C77F,0xB719BB,0xA476D9,0xADC168,0x56E0B4,0x2B705A,0x15B82D,0xF52612,
              0x7A9309,0xC2B380,0x6159C0,0x30ACE0,0x185670,0x0C2B38,0x06159C,0x030ACE,
              0x018567,0xFF38B7,0x80665F,0xBFC92B,0xA01E91,0xAFF54C,0x57FAA6,0x2BFD53,
              0xEA04AD,0x8AF852,0x457C29,0xDD4410,0x6EA208,0x375104,0x1BA882,0x0DD441,
              0xF91024,0x7C8812,0x3E4409,0xE0D800,0x706C00,0x383600,0x1C1B00,0x0E0D80,
              0x0706C0,0x038360,0x01C1B0,0x00E0D8,0x00706C,0x003836,0x001C1B,0xFFF409,
              0x800000,0x400000,0x200000,0x100000,0x080000,0x040000,0x020000,0x010000,
              0x008000,0x004000,0x002000,0x001000,0x000800,0x000400,0x000200,0x000100,
              0x000080,0x000040,0x000020,0x000010,0x000008,0x000004,0x000002,0x000001
              }
#计算幅度的参考值
def com_refer(fifo,begin):
    tempdata=[0]*12
    number=[0]*12
    maxdata=[0]*12
    refer=0
    for i in range(0,3):
        tempdata[i]=fifo[begin+1+i]
        tempdata[i+3] = fifo[begin + 11 + i]
        tempdata[i+6] = fifo[begin + 36+ i]
        tempdata[i+9] = fifo[begin + 46 + i]
    max_num=0
    for i in range(0,12):
        number[i]=0
        for j in range(0,12):
            if tempdata[j]>=tempdata[i]:
                differ=tempdata[j]-tempdata[i]
            else:
                differ = tempdata[i] - tempdata[j]
            if  differ<= tempdata[i] >>3:
                number[i]+=1
        number[i]-=1
        if number[i]>max_num:
             max_num=number[i]
    num=0
    j=0
    for i in range(0,12):
        if number[i]==max_num:
            maxdata[j]=tempdata[i]
            j+=1
            num+=1
    refer=0
    if num==1:
        refer= maxdata[0]
    else:
        for i in range(0,num):
            refer+=maxdata[i]
        refer=refer//num
    return refer

 # 计算代码位和置信度
# 计算代码位和置信度
def  code_confi(refer,data_fifo, ampoba_pt, pulse_amp, confi):
    chip1a_1b=[0]*112
    chip0a_0b=[0]*112
    gate1 = refer + (refer << 1) // 5  # 大于3dB，折算为1.4倍
    gate2 = (refer * 7) // 10          # 小于3dB，折算为0.7倍
    gate3 = refer >> 1
    for i in range(0,112) :
        chip1a_1b[i]=0
        chip0a_0b[i]=0
        #step1
        amp1=data_fifo[ampoba_pt+10*i]
        amp2=data_fifo[ampoba_pt+10*i+5]
        if amp1<=gate1 and amp1>=gate2:
            chip1a_1b[i]+=1
        elif amp1<=gate3:
            chip1a_1b[i]-=1
        if amp2 <=gate1 and amp2>=gate2:
            chip0a_0b[i] += 1
        elif amp2<=gate3:
            chip0a_0b[i] -= 1
         # step2
        amp1 = data_fifo[ampoba_pt + 10 * i+1]
        amp2 = data_fifo[ampoba_pt + 10 * i + 6]
        if amp1 <= gate1 and amp1 >= gate2:
            chip1a_1b[i] += 2
        elif amp1 <= gate3:
            chip1a_1b[i] -= 2
        if amp2 <= gate1 and amp2 >= gate2:
            chip0a_0b[i] += 2
        elif amp2 <= gate3:
            chip0a_0b[i] -= 2
        # step3
        amp1 = data_fifo[ampoba_pt + 10 * i + 2]
        amp2 = data_fifo[ampoba_pt + 10 * i + 7]
        if amp1 <= gate1 and amp1 >= gate2:
            chip1a_1b[i] += 2
        elif amp1 <= gate3:
            chip1a_1b[i] -= 2
        if amp2 <= gate1 and amp2 >= gate2:
            chip0a_0b[i] += 2
        elif amp2 <= gate3:
            chip0a_0b[i] -= 2
        # step4
        amp1 = data_fifo[ampoba_pt + 10 * i + 3]
        amp2 = data_fifo[ampoba_pt + 10 * i + 8]
        if amp1 <= gate1 and amp1 >= gate2:
            chip1a_1b[i] += 2
        elif amp1 <= gate3:
            chip1a_1b[i] -= 2
        if amp2 <= gate1 and amp2 >= gate2:
            chip0a_0b[i] += 2
        elif amp2 <= gate3:
            chip0a_0b[i] -= 2
        # step5
        amp1 = data_fifo[ampoba_pt + 10 * i + 4]
        amp2 = data_fifo[ampoba_pt + 10 * i + 9]
        if amp1 <= gate1 and amp1 >= gate2:
            chip1a_1b[i] += 1
        elif amp1 <= gate3:
            chip1a_1b[i] -= 1
        if amp2 <= gate1 and amp2 >= gate2:
            chip0a_0b[i] +=1
        elif amp2 <= gate3:
            chip0a_0b[i] -= 1
    for i in range(1,112):
        differ=(chip1a_1b[i]-chip0a_0b[i])<<1
        if differ>0:
            pulse_amp[i]=1
        else:
            pulse_amp[i]=0
        if differ<=-6 or differ >=4:
            confi[i]=1
        else:
            confi[i]=0
            temp1=data_fifo[ampoba_pt+10*i]+data_fifo[ampoba_pt+10*i+1]+data_fifo[ampoba_pt+10*i+2]+data_fifo[ampoba_pt+10*i+3]+data_fifo[ampoba_pt+10*i+4]
            temp2 = data_fifo[ampoba_pt + 10 * i+5] + data_fifo[ampoba_pt + 10 * i +6] + data_fifo[ampoba_pt + 10 * i + 7] +  data_fifo[ampoba_pt + 10 * i +8] + data_fifo[ampoba_pt + 10 * i + 9]
            if temp1>temp2:
                pulse_amp[i]=1
            else:
                pulse_amp[i]=0
            if temp1>=(temp2*3>>4+temp2) or  temp2>=(temp1*3>>4+temp1):
                confi[i]=1
# CRC校验
def  crc_check(pulse_amp, correct):
    delay=0;
    for i in range(0,88):
        out=pulse_amp[i]
        out^=delay
        out^=delay>>1
        out ^= delay >> 2
        out^=delay>>3
        out ^= delay >> 4
        out^=delay>>5
        out ^= delay >> 6
        out ^= delay >> 7
        out ^= delay >> 8
        out ^= delay >> 9
        out ^= delay >> 10
        out ^= delay >> 11
        out ^= delay >> 13
        out ^= delay >> 20
        out ^= delay >> 23
        out=out&0x00000001
        delay=delay<<1 & 0x00ffffff
        delay=delay|out;
    for i in range(0,24):
        out=pulse_amp[i+88]
        out ^= delay
        out ^= delay >> 1
        out ^= delay >> 2
        out ^= delay >> 3
        out ^= delay >> 4
        out ^= delay >> 5
        out ^= delay >> 6
        out ^= delay >> 7
        out ^= delay >> 8
        out ^= delay >> 9
        out ^= delay >> 10
        out ^= delay >> 11
        out ^= delay >> 13
        out ^= delay >> 20
        out ^= delay >> 23
        out = out & 0x00000001
        correct[i]=out
        delay = delay << 1 & 0x00ffffff
    for i in range(0,24):
        if correct[i]!=0:
            crc_flag=1
            break
    if crc_flag==0:
        return True
    else:
        return False
#纠错
def err_corr(confi, pulse_amp, correct, syndrome):
    low_con_use=[0]*5
    index=[0]*112
    syn_temp=[0]*112
    for i in range(0,24):
        tp=correct[i]
        tp=tp<<(23-i)
        correct_tp=correct_tp|tp
    correct_tp=correct_tp&0xffffff
    if correct_tp==0:
        plane=0
    else:
        num=0
        flag=0
        for i in range(0,112):
            if confi[i]==0:
                index[i]=i
                syn_temp[j]=syndrome[i]
                num+=1
                j+=1
        if num==0 or num >5:
            plane=1
        else:
            #1
            if num>=1:
                for i in range(0,5):
                    if syn_temp[i]==correct_tp:
                        flag=1
                        low_con_use[i]=1
            #2
            if flag==0 and num>=2:
                tp=syn_temp[0]^syn_temp[1]
                if tp==correct_tp:
                    flag=1
                    low_con_use[0]=1
                    low_con_use[1]=1
                tp = syn_temp[0] ^ syn_temp[2]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[2] = 1
                tp = syn_temp[0] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[3] = 1
                tp = syn_temp[0] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[4] = 1
                tp = syn_temp[1] ^ syn_temp[2]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                tp = syn_temp[1] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[3] = 1
                tp = syn_temp[1] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[4] = 1
                tp = syn_temp[2] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                tp = syn_temp[2] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[2] = 1
                    low_con_use[4] = 1
                tp = syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
            # 3
            if flag == 0 and num >= 3:
                tp = syn_temp[0] ^ syn_temp[1]^ syn_temp[2]
                if tp == correct_tp:
                     flag = 1
                     low_con_use[0] = 1
                     low_con_use[1] = 1
                     low_con_use[2] = 1
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[1] = 1
                    low_con_use[3] = 1
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[1] = 1
                    low_con_use[4] = 1
                tp = syn_temp[0] ^ syn_temp[2] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                tp = syn_temp[0] ^ syn_temp[2] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[2] = 1
                    low_con_use[4] = 1
                tp = syn_temp[0] ^ syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
                tp = syn_temp[1] ^ syn_temp[2] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                tp = syn_temp[1] ^ syn_temp[2] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                    low_con_use[4] = 1
                tp = syn_temp[1] ^ syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
                tp = syn_temp[2] ^ syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
            #4
            if  flag==0 and num>=4:
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[2] ^ syn_temp[3]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[2] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                    low_con_use[4] = 1
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[1] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
                tp = syn_temp[0] ^ syn_temp[2] ^ syn_temp[3] ^ syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[0] = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
                tp =  syn_temp[1] ^ syn_temp[2] ^ syn_temp[3]^syn_temp[4]
                if tp == correct_tp:
                    flag = 1
                    low_con_use[1] = 1
                    low_con_use[2] = 1
                    low_con_use[3] = 1
                    low_con_use[4] = 1
            # 5
            if flag == 0 and num == 5:
                tp = syn_temp[0] ^ syn_temp[1] ^ syn_temp[2] ^ syn_temp[3]^syn_temp[4]
                if tp == correct_tp:
                     flag = 1
                     low_con_use[1] = 1
                     low_con_use[2] = 1
                     low_con_use[3] = 1
                     low_con_use[4] = 1
                     low_con_use[5] = 1
            if flag==0:
                plane=1
            else:
                plane=0
                for i in range(0,num):
                    if low_con_use[i]==1:
                        if pulse_amp[index[i]] ==0:
                            pulse_amp[index[i]] =1
                        else :
                            pulse_amp[index[i]] = 0
    return plane
#解包定义
def  packet(pulse_amp):
    print(1)



#main
temp=[]
file_object = open('1.txt')
i=0
while True:
      all_the_text = file_object.read(10)
      if all_the_text=='':
          break
     # data_fifo.append(int(all_the_text, 16) & 0xFFFF)
     # data_fifo.append(int(all_the_text, 16) // 0xFFFF)
      data_fifo[i]=int(all_the_text, 16) & 0xFFFF
      i+=1
      data_fifo[i] = int(all_the_text, 16) // 0xFFFF
      i+=1
      all_the_text = file_object.read(1)
      if all_the_text=='' or i>1280:
          break
file_object.close()
for i in range(len(data_fifo)):
      if data_fifo[i]==0x5a5a and data_fifo[i+1]==0x5a5a and data_fifo[i+2]== 0xffff:
            data_head = 1
            data_begin = i + 4
            break
if data_head == 1:
    temp.append(data_fifo[data_begin + 2]+ data_fifo[data_begin + 12] +data_fifo[data_begin + 37] + data_fifo[data_begin + 47]+data_fifo[data_begin + 82])
    temp.append(data_fifo[data_begin + 7] + data_fifo[data_begin + 17] +data_fifo[data_begin + 42] + data_fifo[data_begin + 52]+data_fifo[data_begin + 87])
    if temp[0]>temp[1]:
        data_begin = data_begin + 3
    else:
        data_begin = data_begin + 2
    sumoba_pt=data_begin
    ampoba_pt=data_begin+80
    refer_amp=com_refer(data_fifo,sumoba_pt)#计算幅度的参考值
    code_confi(refer_amp,data_fifo, ampoba_pt, pulse_amp, confi); # 计算代码位和置信度
    crc_check_flag = crc_check(pulse_amp, correct); # CRC校验
    if crc_check_flag==0:
        plane_ok=0
    else:
        plane_ok=err_corr(confi,pulse_amp,correct,syndrome)
    if  plane_ok==0:
        packet(pulse_amp)
    exit()